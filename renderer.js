const url = process.env.ORIONX_ENDPOINT || 'https://api2.orionx.io/graphql'
const request = require('request');
const crypto = require('crypto');

const numberWithDots = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
};

function getQuery(coinType) {
    return `{
      market(code: "${coinType}") {
        lastTrade {
          price
        }
      }
      marketCurrentStats(marketCode: "${coinType}", aggregation: d1) {
  		  open
  		  close
  		  high
  		  low
  		  variation
          volume
      }
    }`;
}

function getOrionXPrice(coinType) {
    return new Promise((resolve, reject) => {
        const query = getQuery(coinType);
        const timestamp = new Date().getTime() / 1000;
        const postData = JSON.stringify({ query })
        request
            .post({
                url: url,
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': postData.length,
                    'X-ORIONX-TIMESTAMP': timestamp, // Marca de tiempo actual
                    'X-ORIONX-APIKEY': process.env.ORIONX_APIKEY, // API Key
                    'X-ORIONX-SIGNATURE': crypto
                        .createHmac('sha512', process.env.ORIONX_SECRET_KEY)
                        .update(`${timestamp}${postData}`)
                        .digest('hex'), //  Signature
                },
                body: postData
            }, (err, res, body) => {
                if (err) return reject(err)
                if (res.statusCode !== 200) {
                    return reject(new Error(`Bad statusCode: ${res.statusCode}`))
                }
                try {
                    const json = JSON.parse(body);
                    if (json.data) {
                        resolve({
                            price: json.data.market.lastTrade.price,
                            open: json.data.marketCurrentStats.open,
                            close: json.data.marketCurrentStats.close,
                            high: json.data.marketCurrentStats.high,
                            low: json.data.marketCurrentStats.low,
                            variation: json.data.marketCurrentStats.variation,
                            volume: json.data.marketCurrentStats.volume
                        });
                    } else {
                        reject();
                    }
                } catch (e) {
                    reject(e);
                }
            });
    });
}

const coins = ['CHACLP'];
const types = ['price', 'open', 'close', 'high', 'low'];
function check() {
    coins.forEach(coin => {
        getOrionXPrice(coin)
            .then(result => {
                types.forEach(type => {
                    document.getElementById(coin + '-' + type).innerHTML = numberWithDots(result[type]);
                });
                document.getElementById(coin + '-variation').innerHTML = (result.variation * 100).toFixed(2) + '%';
                document.getElementById(coin + '-volume').innerHTML = (result.volume / 100000000).toFixed(2);
            })
            .catch(err => {
                console.log(err);
            });
    });
}

check(); // Check at startup ;)
setInterval(function () {
    check();
}, 2 * 60 * 1000); // Check every 2 minutes
